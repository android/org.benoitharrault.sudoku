#!/usr/bin/env bash

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

# Fetch grid from game templates
GRIDS="$(cat "${CURRENT_DIR}/../assets/files/templates.json" | grep "0" | sed 's/"//g' | sed 's/,//g')"

function get_block_size() {
  BLOCK_SIZE=""

  GRID_LENGTH="$(echo -n "${GRID}" | wc -c)"
  if [ "${GRID_LENGTH}" == "16" ]; then
    BLOCK_SIZE="2x2"
  fi
  if [ "${GRID_LENGTH}" == "36" ]; then
    BLOCK_SIZE="3x2"
  fi
  if [ "${GRID_LENGTH}" == "81" ]; then
    BLOCK_SIZE="3x3"
  fi
  if [ "${GRID_LENGTH}" == "256" ]; then
    BLOCK_SIZE="4x4"
  fi

  echo "${BLOCK_SIZE}"
}

for GRID in ${GRIDS}; do
  BLOCK_SIZE="$(get_block_size "${GRID}")"
  echo "GRID: ${BLOCK_SIZE} / ${GRID}"
  python ${CURRENT_DIR}/solve.py ${BLOCK_SIZE} ${GRID} | tail -n 1
done

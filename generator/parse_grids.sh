#!/usr/bin/env bash

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

# Generated grids (source)
GRIDS_FILE="$1"

# Game templates
GAME_TEMPLATES_FILE="${CURRENT_DIR}/../assets/files/templates.json"

# Parameters
ALLOWED_BLOCK_SIZE_VALUES="2x2 3x2 3x3 4x4"
MAX_GRIDS_COUNT_PER_LEVEL=20

# Fetch grid from input file
VALID_GRIDS="$(cat "${GRIDS_FILE}" | grep -v "FAILED" | grep "0" | sort | sed 's/ /_/g')"

OUTPUT="{\"templates\":{"
FIRST_BLOCK=1

function clean_grids() {
    GRIDS_TO_CLEAN="$1"
    echo "${GRIDS_TO_CLEAN}" | cut -d"_" -f4 | sed 's/^/"/' | sed 's/$/"/' | tr '\n' ',' | sed 's/,$//'
}

for BLOCK_SIZE in ${ALLOWED_BLOCK_SIZE_VALUES}; do
    GRIDS=$(echo "${VALID_GRIDS}" | grep "${BLOCK_SIZE}");
    GRIDS_COUNT=$(echo "${GRIDS}" | wc -l | awk '{print $1}')
    echo "${BLOCK_SIZE}: found ${GRIDS_COUNT} grids"

    # example with 100 grids, 10 per level:
    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    # |            <- 1/3 ->           |            <- 1/3 ->           |            <- 1/3 ->           |
    # |                                |                                |                                |
    # 1111111111------------------2222222222-----------------------3333333333-------------------4444444444
    # |   10   |                  |   10   |                       |   10   |                   |   10   |

    # easy
    GRIDS_LEVEL_1="$(echo "${GRIDS}" | head -n ${MAX_GRIDS_COUNT_PER_LEVEL})"
    # medium
    GRIDS_LEVEL_2="$(echo "${GRIDS}" | head -n $(echo "${GRIDS_COUNT}/3 - ${MAX_GRIDS_COUNT_PER_LEVEL}/2" | bc) | tail -n ${MAX_GRIDS_COUNT_PER_LEVEL})"
    # hard
    GRIDS_LEVEL_3="$(echo "${GRIDS}" | tail -n $(echo "${GRIDS_COUNT}/3 + ${MAX_GRIDS_COUNT_PER_LEVEL}/2" | bc) | head -n ${MAX_GRIDS_COUNT_PER_LEVEL})"
    # nightmare
    GRIDS_LEVEL_4="$(echo "${GRIDS}" | tail -n ${MAX_GRIDS_COUNT_PER_LEVEL})"

    if [ $FIRST_BLOCK -eq 1 ]; then
        FIRST_BLOCK=0
    else
        OUTPUT="${OUTPUT},"
    fi

    OUTPUT="${OUTPUT} \"${BLOCK_SIZE}\": {"

    OUTPUT="${OUTPUT} \"easy\": [$(clean_grids "${GRIDS_LEVEL_1}")],"
    OUTPUT="${OUTPUT} \"medium\": [$(clean_grids "${GRIDS_LEVEL_2}")],"
    OUTPUT="${OUTPUT} \"hard\": [$(clean_grids "${GRIDS_LEVEL_3}")],"
    OUTPUT="${OUTPUT} \"nightmare\": [$(clean_grids "${GRIDS_LEVEL_4}")]"

    OUTPUT="${OUTPUT} }"
done

OUTPUT="${OUTPUT} }}"

echo ${OUTPUT} | jq > "${GAME_TEMPLATES_FILE}"

echo "Ok, done. Grids saved in ${GAME_TEMPLATES_FILE}"

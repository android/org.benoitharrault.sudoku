#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import sys
from random import randint, shuffle

if (len(sys.argv) != 3):
    printlog('Usage: generate.py block-size difficulty')
    printlog('block-size: [2x2|3x2|3x3|4x4]')
    printlog('difficulty: [easy|medium|hard|nightmare]')
    exit()

blocksize, difficulty = sys.argv[1], sys.argv[2]

if blocksize not in ['2x2', '3x2', '3x3', '4x4']:
    printlog('wrong size given')
    exit()

splitted_blocksize = blocksize.split('x')
size_horizontal = int(splitted_blocksize[0])
size_vertical = int(splitted_blocksize[1])

boardSize = size_horizontal * size_vertical

if difficulty not in ['easy', 'medium', 'hard', 'nightmare']:
    printlog('wrong difficulty given')
    exit()

debugFillGrid = False
debugSolveGrid = False
debugComputeGameGrid = True

############################################################################

#
# Difficulty grid:
# (number of "force/retry" during grid generation)
#
#   | Size | H+V | Easy |   Medium   |     Hard    |  Nightmare  |
#   | :--: | --: | ---: | ---------- | ----------- | ----------- |
#   |  2x2 |   4 |    1 | 12 - 4 = 8 | 15 - 4 = 11 | 18 - 4 = 14 |
#   |  2x3 |   5 |    1 | 12 - 5 = 7 | 15 - 5 = 10 | 18 - 5 = 13 |
#   |  3x2 |   5 |    1 | 12 - 5 = 7 | 15 - 5 = 10 | 18 - 5 = 13 |
#   |  3x3 |   6 |    1 | 12 - 6 = 6 | 15 - 6 =  9 | 18 - 6 = 12 |
#   |  4x4 |   8 |    1 | 12 - 8 = 4 | 15 - 8 =  7 | 18 - 8 = 10 |
#

difficultyLevel = 1
if difficulty == 'easy':
    difficultyLevel = 1
if difficulty == 'medium':
    difficultyLevel = 12 - (size_horizontal + size_vertical)
if difficulty == 'hard':
    difficultyLevel = 15 - (size_horizontal + size_vertical)
if difficulty == 'nightmare':
    difficultyLevel = 18 - (size_horizontal + size_vertical)

sys.stdout.write('Will generate grid: [' + str(size_horizontal) + 'x' + str(
    size_vertical) + '], difficulty: ' + difficulty + ' (level ' + str(difficultyLevel) + ')\n')

stringValues = '0123456789ABCDEFG'


# draw grid (array style)
def drawGrid(grid):
    gridVerticalSize = len(grid)
    gridHorizontalSize = len(grid[0])
    horizontalLineLength = ((size_horizontal + 1) * size_vertical) + 1

    for row in range(gridHorizontalSize):
        if ((row % size_vertical) == 0):
            sys.stdout.write(('═' * horizontalLineLength) + '\n')
        for col in range(gridVerticalSize):
            if ((col % size_horizontal) == 0):
                sys.stdout.write('║')
            if grid[row][col] != 0:
                sys.stdout.write(stringValues[grid[row][col]])
            else:
                sys.stdout.write(' ')
        sys.stdout.write('║\n')
    sys.stdout.write(('═' * horizontalLineLength) + '\n')
    sys.stdout.write('\n')


# draw grid (inline style)
def drawGridInline(grid):
    for row in range(len(grid)):
        for col in range(len(grid[row])):
            sys.stdout.write(stringValues[grid[row][col]])
    sys.stdout.write('\n')


# initialise empty grid
def generateEmptyGrid(boardSize):
    emptyGrid = []
    for row in range(boardSize):
        emptyGrid.append([])
        for col in range(boardSize):
            emptyGrid[row].append(0)
    return emptyGrid


# A check if the grid is full
def checkFullyCompletedGrid(grid):
    for row in range(len(grid)):
        for col in range(len(grid[row])):
            if grid[row][col] == 0:
                return False
    return True


# (deep) copy of grid
def copyGrid(grid):
    copiedGrid = []
    for row in range(len(grid)):
        copiedGrid.append([])
        for col in range(len(grid[row])):
            copiedGrid[row].append(grid[row][col])
    return copiedGrid


# A backtracking/recursive function to check all
# possible combinations of numbers until a solution is found
def solveGrid(grid, iterationSolveCount):
    if debugSolveGrid:
        sys.stdout.write('solveGrid / ' + str(iterationSolveCount) + '\n')
    gridSize = len(grid)
    cellsCount = len(grid) * len(grid[0])
    numberList = [(value + 1) for value in range(gridSize)]

    global solutionsCount

    # Find next empty cell
    for i in range(0, cellsCount):
        row = i // gridSize
        col = i % gridSize
        if grid[row][col] == 0:
            shuffle(numberList)
            for value in numberList:
                if debugSolveGrid:
                    sys.stdout.write(
                        'solveGrid: '
                        + '[' + str(row) + ',' + str(col) + ']'
                        + ' try with value ' + str(value)
                        + '\n'
                    )
                # Check that this value has not already be used on this row
                if not(value in grid[row]):
                    # Check that this value has not already be used on this column
                    foundInColumn = False
                    for r in range(0, gridSize):
                        if (value == grid[r][col]):
                            foundInColumn = True

                    if not foundInColumn:
                        # Get sub-square
                        blockColFrom = size_horizontal * \
                            int(col / size_horizontal)
                        blockRowFrom = size_vertical * int(row / size_vertical)
                        square = [grid[i][blockColFrom:blockColFrom + size_horizontal]
                                  for i in range(blockRowFrom, blockRowFrom + size_vertical)]

                        # Check that this value has not already be used on this sub square
                        if not any(value in squareLine for squareLine in square):
                            grid[row][col] = value
                            if checkFullyCompletedGrid(grid):
                                if debugSolveGrid:
                                    sys.stdout.write(
                                        'solveGrid: grid complete, found solution\n')
                                iterationSolveCount += 1
                                solutionsCount += 1
                                break
                            else:
                                if debugSolveGrid:
                                    sys.stdout.write('solveGrid: recursive call (solutionsCount=' + str(
                                        solutionsCount) + ', iterationSolveCount=' + str(iterationSolveCount) + ')\n')
                                if solveGrid(grid, iterationSolveCount + 1):
                                    if debugSolveGrid:
                                        sys.stdout.write(
                                            'solveGrid: still searching for solution\n')
                                    return True
            break
    grid[row][col] = 0


# A backtracking/recursive function to check all possible combinations of numbers until a solution is found
def fillGrid(grid, boardSize, iterationFillCount):
    if debugFillGrid:
        sys.stdout.write('fillGrid / ' + str(iterationFillCount) + '\n')
        drawGrid(grid)

    boardSize = len(grid)
    cellsCount = len(grid) * len(grid[0])
    numberList = [(value + 1) for value in range(boardSize)]

    global solutionsCount

    # Find next empty cell
    for i in range(0, cellsCount):
        row = i // boardSize
        col = i % boardSize

        # Ensure cell is not already set
        if grid[row][col] == 0:
            # Try to fill cell with random numbers, iteratively
            shuffle(numberList)
            for value in numberList:
                if debugFillGrid:
                    sys.stdout.write(
                        'fillGrid: [' + str(row) + ',' + str(col) + '] -> try with value ' + str(value) + '\n')
                # Check that this value has not already be used on this row
                if not(value in grid[row]):
                    # Check that this value has not already be used on this column
                    foundInColumn = False
                    for r in range(0, boardSize):
                        if (value == grid[r][col]):
                            foundInColumn = True

                    if not foundInColumn:
                        # Get sub-square
                        blockColFrom = size_horizontal * \
                            int(col / size_horizontal)
                        blockRowFrom = size_vertical * int(row / size_vertical)
                        square = [grid[i][blockColFrom:blockColFrom + size_horizontal]
                                  for i in range(blockRowFrom, blockRowFrom + size_vertical)]

                        # Check that this value has not already be used on this sub square
                        if not any(value in squareLine for squareLine in square):
                            if debugFillGrid:
                                sys.stdout.write(
                                    'fillGrid: [' + str(row) + ',' + str(col) + '] <- ' + str(value) + ' / ok, no conflict\n')
                            grid[row][col] = value
                            if checkFullyCompletedGrid(grid):
                                if debugFillGrid:
                                    sys.stdout.write(
                                        'fillGrid: found final solution\n')
                                return True
                            else:
                                if debugFillGrid:
                                    sys.stdout.write(
                                        'fillGrid: recursive call (iterationFillCount=' + str(iterationFillCount) + ')\n')
                                iterationFillCount += 1
                                if fillGrid(grid, boardSize, iterationFillCount):
                                    return True
            break
    if debugFillGrid:
        sys.stdout.write(
            'fillGrid: no solution found [' + str(row) + ',' + str(col) + '] <- 0\n')
    grid[row][col] = 0


solutionsCount = 1


def computeResolvableGrid(grid, maxAttemps):
    global solutionsCount

    # A higher number of attemps will end up removing more numbers from the grid
    # Potentially resulting in more difficiult grids to solve!

    # Start Removing Numbers one by one
    remainingAttemps = maxAttemps
    while remainingAttemps > 0:
        if debugComputeGameGrid:
            sys.stdout.write(
                'computeResolvableGrid / remainingAttemps: ' + str(remainingAttemps) + '.\n')

        # Select a random cell that is not already empty
        row = randint(0, boardSize - 1)
        col = randint(0, boardSize - 1)
        while grid[row][col] == 0:
            row = randint(0, boardSize - 1)
            col = randint(0, boardSize - 1)

        # Remove value in this random cell
        savedCellValue = grid[row][col]
        grid[row][col] = 0

        solutionsCount = 0
        if debugComputeGameGrid:
            sys.stdout.write('computeResolvableGrid / Remove value in [' + str(
                row) + ',' + str(col) + '] (was ' + str(savedCellValue) + ').\n')
            drawGrid(grid)
            sys.stdout.write(
                'computeResolvableGrid / Check grid unique solution...\n')

        solveGrid(copyGrid(grid), 0)

        # Non unique solution => restore this cell value
        if solutionsCount != 1:
            if debugComputeGameGrid:
                sys.stdout.write(
                    'computeResolvableGrid / Failed to solve grid (multiple solutions). Will try with clearing another cell.\n')
            grid[row][col] = savedCellValue
            remainingAttemps -= 1
        else:
            if debugComputeGameGrid:
                sys.stdout.write(
                    'computeResolvableGrid / ok found unique solution.\n')

    if debugComputeGameGrid:
        sys.stdout.write('computeResolvableGrid / ok found solvable grid.\n')

###########################################################################


grid = generateEmptyGrid(boardSize)

sys.stdout.write('Building grid...\n')
fillGrid(grid, boardSize, 0)

sys.stdout.write('Solved grid:\n')
drawGrid(grid)

sys.stdout.write('Generating solvable grid:\n')
computeResolvableGrid(grid, difficultyLevel)

sys.stdout.write('Generated grid:\n')
drawGrid(grid)

sys.stdout.write(
    'Inline grid [' + str(size_horizontal) + 'x' + str(size_vertical) + ']'
    + ', '
    + 'difficulty: ' + difficulty
    + ' '
    + '(level ' + str(difficultyLevel) + ')'
    + ':'
    + '\n'
)
drawGridInline(grid)

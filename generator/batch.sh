#!/usr/bin/env bash

# This script should be run and redirected to a file like "batch.sh > grids.log"
# Then this grids file should be imported in "parse_grids.sh" to generate json

CURRENT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

ALLOWED_BLOCK_SIZE_VALUES="2x2 3x2 3x3 4x4"
ALLOWED_DIFFICULTY_VALUES="easy medium hard nightmare"
GRIDS_COUNT=40

for BLOCK_SIZE in ${ALLOWED_BLOCK_SIZE_VALUES}; do
  CELLS_COUNT=$(echo "(${BLOCK_SIZE})^2" | sed 's/x/\*/g' | bc)
  for DIFFICULTY in ${ALLOWED_DIFFICULTY_VALUES}; do
    echo "Block size: ${BLOCK_SIZE} / Difficulty: ${DIFFICULTY}"
    for i in $(seq ${GRIDS_COUNT}); do
      # Generate grid candidate
      GRID="$(python ${CURRENT_DIR}/generate.py ${BLOCK_SIZE} ${DIFFICULTY} | tail -n 1)"

      # Ensure grid can be resolved without any assumption
      CAN_BE_SOLVED="$(python ${CURRENT_DIR}/solve.py ${BLOCK_SIZE} ${GRID} | tail -n 1)"
      if [ "${CAN_BE_SOLVED}" == "Ok" ]; then
        # Count "0" in grid, compute "emptiness" ratio
        STRING="${GRID//[^0]}"
        ZEROS_COUNT=$(echo "${#STRING}")
        RATIO=$(echo "100*${ZEROS_COUNT}/${CELLS_COUNT}" | bc)
        RATIO_STRING="$(printf "%02d" ${RATIO})"

        echo "${BLOCK_SIZE} (${RATIO_STRING}%) ${DIFFICULTY} ${GRID}"
      else
        echo "FAILED / ${BLOCK_SIZE} ${DIFFICULTY}"
      fi
    done
  done
done

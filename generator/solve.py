#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

#
# Naive sudoku grid solver
#
# Will try to solve sudoku grid without making any assumption
# -> pick and fill first cell where only one value is allowed
#    and loop until grid is filled
#
# Will fail if solution needs any assumption on a cell value
#

import sys

if (len(sys.argv) != 3):
    printlog('Usage: solve.py block-size grid')
    printlog('block-size: [2x2|3x2|3x3|4x4]')
    exit()

blocksize, gridTemplate = sys.argv[1], sys.argv[2]

if blocksize not in ['2x2', '3x2', '3x3', '4x4']:
    printlog('wrong size given')
    exit()

splitted_blocksize = blocksize.split('x')
size_horizontal = int(splitted_blocksize[0])
size_vertical = int(splitted_blocksize[1])

boardSize = size_horizontal * size_vertical

if (len(gridTemplate) != boardSize * boardSize):
    printlog('wrong grid length (should be ' + str(boardSize * boardSize) + ')')
    exit()

debugSolveGrid = False

############################################################################

sys.stdout.write('Will solve grid: [' + str(size_horizontal) +
                 'x' + str(size_vertical) + '] // ' + gridTemplate + '\n')

stringValues = '0123456789ABCDEFG'


# draw grid (array style)
def drawGrid(grid):
    gridVerticalSize = len(grid)
    gridHorizontalSize = len(grid[0])
    horizontalLineLength = ((size_horizontal + 1) * size_vertical) + 1

    for row in range(gridHorizontalSize):
        if ((row % size_vertical) == 0):
            sys.stdout.write(('═' * horizontalLineLength) + '\n')
        for col in range(gridVerticalSize):
            if ((col % size_horizontal) == 0):
                sys.stdout.write('║')
            if grid[row][col] != 0:
                sys.stdout.write(stringValues[grid[row][col]])
            else:
                sys.stdout.write(' ')
        sys.stdout.write('║\n')
    sys.stdout.write(('═' * horizontalLineLength) + '\n')
    sys.stdout.write('\n')


# (deep) copy of grid
def copyGrid(grid):
    copiedGrid = []
    for row in range(len(grid)):
        copiedGrid.append([])
        for col in range(len(grid[row])):
            copiedGrid[row].append(grid[row][col])
    return copiedGrid


# Init grid from given template
def initGrid(boardSize, gridTemplate):
    grid = []
    index = 0
    for row in range(boardSize):
        grid.append([])
        for col in range(boardSize):
            grid[row].append(stringValues.index(gridTemplate[index]))
            index += 1
    return grid


# Check if grid is fully completed, without any empty cell
def isFullyCompleted(grid):
    for row in range(len(grid)):
        for col in range(len(grid[row])):
            if grid[row][col] == 0:
                return False
    return True


# Check if a list contains duplicates (conflicts)
def containsDuplicates(list):
    tmp_set = set(list)
    return (len(list) != len(tmp_set))


# Check if given grid contains conflicts
def hasConflict(grid, size_horizontal, size_vertical):
    # Check horizontal conflicts
    for row in range(len(grid)):
        values = []
        for col in range(len(grid[row])):
            value = grid[row][col]
            if value != 0:
                values.append(value)
        if containsDuplicates(values):
            # printlog('Horizontal conflict found')
            return True

    # Check vertical conflicts
    for col in range(len(grid[0])):
        values = []
        for row in range(len(grid)):
            value = grid[row][col]
            if value != 0:
                values.append(value)
        if containsDuplicates(values):
            # printlog('Vertical conflict found')
            return True

    # Check sub-blocks conflicts
    for blockRow in range(size_horizontal):
        for blockCol in range(size_vertical):
            # Get sub-square
            values = []
            for rowInBlock in range(size_vertical):
                for colInBlock in range(size_horizontal):
                    row = (blockRow * size_vertical) + rowInBlock
                    col = (blockCol * size_horizontal) + colInBlock
                    value = grid[row][col]
                    if value != 0:
                        values.append(value)
            if containsDuplicates(values):
                # printlog('Sub-block conflict found')
                return True

    return False


# Check if a value is allowed in a cell (without conflicting)
def isValueAllowed(grid, size_horizontal, size_vertical, row, col, candidateValue):
    testGrid = copyGrid(grid)
    testGrid[row][col] = candidateValue
    if not hasConflict(testGrid, size_horizontal, size_vertical):
        return True
    return False


# Get allowed values in a cell (witjout conflicting)
def findAllowedValuesForCell(grid, size_horizontal, size_vertical, row, col):
    allowedValues = []
    if not hasConflict(grid, size_horizontal, size_vertical):
        for candidateValue in range(1, size_horizontal * size_vertical + 1):
            if isValueAllowed(grid, size_horizontal, size_vertical, row, col, candidateValue):
                allowedValues.append(candidateValue)
    return allowedValues


# Globally solve grid
def solve(grid, size_horizontal, size_vertical):
    iterations = 0
    maxIterations = 500

    # Loop until grid is fully completed
    while True:
        iterations += 1
        if isFullyCompleted(grid) or (iterations > maxIterations):
            break

        if debugSolveGrid:
            printlog('===================================')
            printlog('Iteration: ' + str(iterations))

        # Get first/next cell with only one allowed value
        candidates = []
        if debugSolveGrid:
            printlog('Searching for empty cells...')
        for row in range(len(grid)):
            for col in range(len(grid[row])):
                if grid[row][col] == 0:
                    if debugSolveGrid:
                        printlog(
                            'Found empty cell [' + str(col) + ',' + str(row) + ']')
                    candidates.append([row, col])

        if len(candidates):
            for candidate in candidates:
                candidateRow = candidate[0]
                candidateCol = candidate[1]
                allowedValues = findAllowedValuesForCell(
                    grid, size_horizontal, size_vertical, candidateRow, candidateCol)
                if debugSolveGrid:
                    printlog('Allowed values for cell [' + str(candidateCol) + ',' + str(
                        candidateRow) + ']: ' + str(allowedValues))
                if len(allowedValues) != 1:
                    if debugSolveGrid:
                        printlog(' Non unique allowed value for cell. Skip to next cell')
                else:
                    value = allowedValues[0]
                    grid[candidateRow][candidateCol] = value
                    if debugSolveGrid:
                        printlog(' Found unique allowed value for cell [' + str(
                            candidateCol) + ',' + str(candidateRow) + ']: ' + str(value))
                        drawGrid(grid)

#########################


sys.stdout.write('Building start grid:\n')
grid = initGrid(boardSize, gridTemplate)
drawGrid(grid)

sys.stdout.write('Checking grid:\n')
if hasConflict(grid, size_horizontal, size_vertical):
    sys.stdout.write(' - oups, initial conflict found.\n')
else:
    sys.stdout.write(' - ok, no initial conflict found.\n')
    sys.stdout.write('\n')

    sys.stdout.write('Solving grid...\n')
    solve(grid, size_horizontal, size_vertical)

    if isFullyCompleted(grid):
        sys.stdout.write('Ok, solved grid:\n')
        drawGrid(grid)
        sys.stdout.write('Ok\n')
    else:
        sys.stdout.write('Failed to solve grid\n')

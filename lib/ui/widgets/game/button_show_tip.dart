import 'package:badges/badges.dart' as badges;
import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/config/application_config.dart';
import 'package:sudoku/cubit/activity/activity_cubit.dart';
import 'package:sudoku/models/activity/activity.dart';

class ButtonShowTip extends StatelessWidget {
  const ButtonShowTip({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return StyledButton(
          color: Colors.purple,
          child: badges.Badge(
            showBadge: currentActivity.givenTipsCount == 0 ? false : true,
            badgeStyle: badges.BadgeStyle(
              badgeColor: currentActivity.givenTipsCount < 10
                  ? Colors.green
                  : currentActivity.givenTipsCount < 20
                      ? Colors.orange
                      : Colors.red,
            ),
            badgeContent: Text(
              currentActivity.givenTipsCount == 0
                  ? ''
                  : currentActivity.givenTipsCount.toString(),
              style: const TextStyle(color: Colors.white),
            ),
            child: Container(
              padding: EdgeInsets.all(10 *
                  currentActivity.buttonTipsCountdown /
                  ApplicationConfig.defaultTipCountDownValueInSeconds),
              child: const Image(
                image: AssetImage('assets/ui/button_help.png'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          onPressed: () {
            currentActivity.canGiveTip
                ? currentActivity.showTip(BlocProvider.of<ActivityCubit>(context))
                : null;
          },
        );
      },
    );
  }
}

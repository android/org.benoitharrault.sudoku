import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/cubit/activity/activity_cubit.dart';
import 'package:sudoku/models/activity/cell_location.dart';
import 'package:sudoku/models/activity/activity.dart';
import 'package:sudoku/ui/widgets/game/cell.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        final Color borderColor = Theme.of(context).colorScheme.onSurface;

        return Container(
          margin: const EdgeInsets.all(2),
          padding: const EdgeInsets.all(2),
          decoration: BoxDecoration(
            color: borderColor,
            borderRadius: BorderRadius.circular(2),
            border: Border.all(
              color: borderColor,
              width: 2,
            ),
          ),
          child: Column(
            children: [
              Table(
                defaultColumnWidth: const IntrinsicColumnWidth(),
                children: [
                  for (int row = 0; row < currentActivity.boardSize; row++)
                    TableRow(
                      children: [
                        for (int col = 0; col < currentActivity.boardSize; col++)
                          Column(
                            children: [
                              CellWidget(
                                  cell: currentActivity.board.get(CellLocation.go(row, col)))
                            ],
                          ),
                      ],
                    ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}

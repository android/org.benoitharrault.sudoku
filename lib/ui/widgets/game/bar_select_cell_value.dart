import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/cubit/activity/activity_cubit.dart';
import 'package:sudoku/models/activity/cell.dart';
import 'package:sudoku/models/activity/cell_location.dart';
import 'package:sudoku/models/activity/activity.dart';
import 'package:sudoku/ui/widgets/game/cell_update.dart';

class SelectCellValueBar extends StatelessWidget {
  const SelectCellValueBar({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity activity = activityState.currentActivity;

        final bool isUpdatableCellSelected =
            (activity.selectedCell != null) ? !activity.selectedCell!.isFixed : false;
        final int maxValue = activity.boardSize;

        const int maxItemsPerLine = 10;
        final int linesCount = (maxValue / maxItemsPerLine).floor() + 1;
        final int itemsCountPerLine = min(maxItemsPerLine, maxValue + 1);

        return Container(
          margin: const EdgeInsets.all(2),
          padding: const EdgeInsets.all(2),
          child: Table(
            defaultColumnWidth: const IntrinsicColumnWidth(),
            children: [
              for (int lineIndex = 0; lineIndex < linesCount; lineIndex++)
                TableRow(
                  children: [
                    for (int value = lineIndex * itemsCountPerLine;
                        value < (lineIndex + 1) * itemsCountPerLine;
                        value++)
                      Column(
                        children: [
                          CellWidgetUpdate(
                            cell: Cell(
                              location: CellLocation.go(1, value),
                              value: isUpdatableCellSelected
                                  ? (value <= maxValue ? value : -1)
                                  : -1,
                              isFixed: false,
                            ),
                          ),
                        ],
                      ),
                  ],
                ),
            ],
          ),
        );
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/cubit/activity/activity_cubit.dart';
import 'package:sudoku/models/activity/activity.dart';

class ButtonShowConflicts extends StatelessWidget {
  const ButtonShowConflicts({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return StyledButton(
          color: currentActivity.showConflicts == true ? Colors.amber : Colors.grey,
          child: const Image(
            image: AssetImage('assets/ui/button_show_conflicts.png'),
            fit: BoxFit.fill,
          ),
          onPressed: () {
            BlocProvider.of<ActivityCubit>(context).toggleShowConflicts();
          },
        );
      },
    );
  }
}

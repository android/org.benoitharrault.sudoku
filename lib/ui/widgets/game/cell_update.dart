import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:sudoku/config/application_config.dart';

import 'package:sudoku/cubit/activity/activity_cubit.dart';
import 'package:sudoku/models/activity/cell.dart';
import 'package:sudoku/models/activity/activity.dart';

class CellWidgetUpdate extends StatelessWidget {
  const CellWidgetUpdate({super.key, this.cell});

  final Cell? cell;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity activity = activityState.currentActivity;

        if ((cell?.value ?? 0) < 0) {
          return Container();
        }

        final String imageAsset = getImageAssetName(activity);

        Color backgroundColor = Colors.grey.shade200;

        if (activity.showConflicts == true &&
            activity.selectedCell?.location.col != null &&
            activity.selectedCell?.location.row != null) {
          if (!activity.board
              .isValueAllowed(activity.selectedCell?.location, cell?.value ?? 0)) {
            backgroundColor = Colors.pink.shade100;
          }
        }

        return Container(
          decoration: BoxDecoration(
            color: backgroundColor,
            border: Border.all(
              color: Colors.grey.shade700,
              width: 2,
            ),
          ),
          child: GestureDetector(
            child: Image(image: AssetImage(imageAsset), fit: BoxFit.fill),
            onTap: () {
              final ActivityCubit activityCubit = BlocProvider.of<ActivityCubit>(context);

              if (activity.selectedCell != null) {
                activityCubit.updateCellValue(
                    activity.selectedCell!.location, cell?.value ?? 0);
              }

              activityCubit.unselectCell();
            },
          ),
        );
      },
    );
  }

  /*
  * Compute image asset name, from skin and cell value/state
  */
  String getImageAssetName(Activity activity) {
    if ((cell?.value ?? 0) > 0) {
      final int cellValue = activity.getTranslatedValueForDisplay(cell?.value ?? 0);
      return 'assets/skins/${activity.activitySettings.get(ApplicationConfig.parameterCodeSkin)}_$cellValue.png';
    }

    return 'assets/ui/cell_empty.png';
  }
}

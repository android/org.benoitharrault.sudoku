import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/cubit/activity/activity_cubit.dart';
import 'package:sudoku/models/activity/activity.dart';
import 'package:sudoku/ui/widgets/game/button_show_conflicts.dart';
import 'package:sudoku/ui/widgets/game/button_show_tip.dart';

class GameTopWidget extends StatelessWidget {
  const GameTopWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return SizedBox(
          height: 60,
          child: (currentActivity.isRunning && !currentActivity.isFinished)
              ? const Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ButtonShowTip(),
                    ButtonShowConflicts(),
                  ],
                )
              : const SizedBox.shrink(),
        );
      },
    );
  }
}

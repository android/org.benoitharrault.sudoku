import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/cubit/activity/activity_cubit.dart';
import 'package:sudoku/models/activity/activity.dart';
import 'package:sudoku/ui/widgets/game/bar_select_cell_value.dart';

class GameBottomWidget extends StatelessWidget {
  const GameBottomWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return currentActivity.isFinished
            ? const SizedBox.shrink()
            : const SelectCellValueBar();
      },
    );
  }
}

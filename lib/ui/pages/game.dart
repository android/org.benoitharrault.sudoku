import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/cubit/activity/activity_cubit.dart';
import 'package:sudoku/models/activity/activity.dart';
import 'package:sudoku/ui/game/game_bottom.dart';
import 'package:sudoku/ui/game/game_end.dart';
import 'package:sudoku/ui/game/game_top.dart';
import 'package:sudoku/ui/widgets/game/game_board.dart';

class PageGame extends StatelessWidget {
  const PageGame({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final Activity currentActivity = activityState.currentActivity;

        return Container(
          alignment: AlignmentDirectional.topCenter,
          padding: const EdgeInsets.all(4),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const GameTopWidget(),
              const SizedBox(height: 8),
              const GameBoardWidget(),
              const SizedBox(height: 8),
              const GameBottomWidget(),
              const Expanded(child: SizedBox.shrink()),
              currentActivity.isFinished ? const GameEndWidget() : const SizedBox.shrink(),
            ],
          ),
        );
      },
    );
  }
}

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/config/application_config.dart';

class ParameterPainterDifficultyLevel extends CustomPainter {
  const ParameterPainterDifficultyLevel({
    required this.context,
    required this.value,
  });

  final BuildContext context;
  final String value;

  @override
  void paint(Canvas canvas, Size size) {
    // force square
    final double canvasSize = min(size.width, size.height);

    final List<dynamic> stars = [];

    switch (value) {
      case ApplicationConfig.difficultyLevelValueEasy:
        stars.add([0.5, 0.5]);
        break;
      case ApplicationConfig.difficultyLevelValueMedium:
        stars.add([0.3, 0.5]);
        stars.add([0.7, 0.5]);
        break;
      case ApplicationConfig.difficultyLevelValueHard:
        stars.add([0.3, 0.3]);
        stars.add([0.7, 0.3]);
        stars.add([0.5, 0.7]);
        break;
      case ApplicationConfig.difficultyLevelValueNightmare:
        stars.add([0.3, 0.3]);
        stars.add([0.7, 0.3]);
        stars.add([0.3, 0.7]);
        stars.add([0.7, 0.7]);
        break;
      default:
        printlog('Wrong value for level parameter value: $value');
    }

    final paint = Paint();
    paint.strokeJoin = StrokeJoin.round;
    paint.strokeWidth = 3 / 100 * canvasSize;

    // Stars
    final textSpan = TextSpan(
      text: '⭐',
      style: TextStyle(
        color: Colors.black,
        fontSize: canvasSize / 3,
        fontWeight: FontWeight.bold,
      ),
    );
    final textPainter = TextPainter(
      text: textSpan,
      textDirection: TextDirection.ltr,
      textAlign: TextAlign.center,
    );
    textPainter.layout();

    for (var center in stars) {
      textPainter.paint(
        canvas,
        Offset(
          canvasSize * center[0] - textPainter.width * 0.5,
          canvasSize * center[1] - textPainter.height * 0.5,
        ),
      );
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

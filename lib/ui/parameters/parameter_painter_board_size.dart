import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/config/application_config.dart';

class ParameterPainterBoardSize extends CustomPainter {
  const ParameterPainterBoardSize({
    required this.context,
    required this.value,
  });

  final BuildContext context;
  final String value;

  @override
  void paint(Canvas canvas, Size size) {
    // force square
    final double canvasSize = min(size.width, size.height);

    int gridWidth = 1;
    int gridHeight = 1;

    switch (value) {
      case ApplicationConfig.boardSizeValueTiny:
        gridWidth = 2;
        gridHeight = 2;
        break;
      case ApplicationConfig.boardSizeValueSmall:
        gridWidth = 3;
        gridHeight = 2;
        break;
      case ApplicationConfig.boardSizeValueStandard:
        gridWidth = 3;
        gridHeight = 3;
        break;
      case ApplicationConfig.boardSizeValueLarge:
        gridWidth = 4;
        gridHeight = 4;
        break;
      default:
        printlog('Wrong value for size parameter value: $value');
    }

    final paint = Paint();
    paint.strokeJoin = StrokeJoin.round;
    paint.strokeWidth = 3;

    // Mini grid
    final borderColor = Colors.grey.shade800;

    final double cellSize = canvasSize / 5;
    final double originX = (canvasSize - gridWidth * cellSize) / 2;
    final double originY = (canvasSize - gridHeight * cellSize) / 2;

    for (int row = 0; row < gridHeight; row++) {
      for (int col = 0; col < gridWidth; col++) {
        final Offset topLeft = Offset(originX + col * cellSize, originY + row * cellSize);
        final Offset bottomRight = topLeft + Offset(cellSize, cellSize);

        paint.color = Colors.white;
        paint.style = PaintingStyle.fill;
        canvas.drawRect(Rect.fromPoints(topLeft, bottomRight), paint);

        paint.color = borderColor;
        paint.style = PaintingStyle.stroke;
        canvas.drawRect(Rect.fromPoints(topLeft, bottomRight), paint);
      }
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

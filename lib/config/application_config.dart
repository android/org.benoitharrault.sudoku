import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/cubit/activity/activity_cubit.dart';

import 'package:sudoku/ui/pages/game.dart';

import 'package:sudoku/ui/parameters/parameter_painter_board_size.dart';
import 'package:sudoku/ui/parameters/parameter_painter_difficulty_level.dart';

class ApplicationConfig {
  // activity parameter: difficulty level
  static const String parameterCodeDifficultyLevel = 'activity.difficultyLevel';
  static const String difficultyLevelValueEasy = 'easy';
  static const String difficultyLevelValueMedium = 'medium';
  static const String difficultyLevelValueHard = 'hard';
  static const String difficultyLevelValueNightmare = 'nightmare';

  // activity parameter: board size
  static const String parameterCodeBoardSize = 'activity.boardSize';
  static const String boardSizeValueTiny = '2x2';
  static const String boardSizeValueSmall = '3x2';
  static const String boardSizeValueStandard = '3x3';
  static const String boardSizeValueLarge = '4x4';

  // activity parameter: skin
  static const String parameterCodeSkin = 'global.skin';
  static const String skinValueDigits = 'digits';
  static const String skinValueFood = 'food';
  static const String skinValueNature = 'nature';
  static const String skinValueMonsters = 'monsters';

  static const List<String> shufflableSkins = [
    skinValueFood,
    skinValueNature,
    skinValueMonsters,
  ];

  static const int defaultTipCountDownValueInSeconds = 20;

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Sudoku',
    activitySettings: [
      // difficulty level
      ApplicationSettingsParameter(
        code: parameterCodeDifficultyLevel,
        values: [
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueEasy,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueMedium,
            color: Colors.orange,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueHard,
            color: Colors.red,
          ),
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelValueNightmare,
            color: Colors.purple,
          ),
        ],
        customPainter: (context, value) => ParameterPainterDifficultyLevel(
          context: context,
          value: value,
        ),
      ),

      // board size
      ApplicationSettingsParameter(
        code: parameterCodeBoardSize,
        values: [
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueTiny,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueSmall,
            color: Colors.orange,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueStandard,
            color: Colors.red,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueLarge,
            color: Colors.purple,
          ),
        ],
        customPainter: (context, value) => ParameterPainterBoardSize(
          context: context,
          value: value,
        ),
      ),

      // skin
      ApplicationSettingsParameter(
        code: parameterCodeSkin,
        values: [
          ApplicationSettingsParameterItemValue(
            value: skinValueDigits,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: skinValueFood,
          ),
          ApplicationSettingsParameterItemValue(
            value: skinValueNature,
          ),
          ApplicationSettingsParameterItemValue(
            value: skinValueMonsters,
          ),
        ],
        builder: ({
          required context,
          required itemValue,
          required onPressed,
          required size,
        }) =>
            StyledButton(
          color: Colors.green.shade800,
          onPressed: onPressed,
          child: Image(
            image: AssetImage('assets/ui/skin_${itemValue.value}.png'),
            fit: BoxFit.fill,
          ),
        ),
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      appBarConfiguration: AppBarConfiguration(
        hideApplicationTitle: true,
        pushQuitActivityButtonLeft: true,
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );
}

import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/config/application_config.dart';
import 'package:sudoku/models/activity/cell.dart';
import 'package:sudoku/models/activity/cell_location.dart';
import 'package:sudoku/models/activity/activity.dart';
import 'package:sudoku/utils/board_animate.dart';

part 'activity_state.dart';

class ActivityCubit extends HydratedCubit<ActivityState> {
  ActivityCubit()
      : super(ActivityState(
          currentActivity: Activity.createEmpty(),
        ));

  void updateState(Activity activity) {
    emit(ActivityState(
      currentActivity: activity,
    ));
  }

  void refresh() {
    final Activity activity = Activity(
      // Settings
      activitySettings: state.currentActivity.activitySettings,
      // State
      isRunning: state.currentActivity.isRunning,
      isStarted: state.currentActivity.isStarted,
      isFinished: state.currentActivity.isFinished,
      animationInProgress: state.currentActivity.animationInProgress,
      boardAnimated: state.currentActivity.boardAnimated,
      // Base data
      board: state.currentActivity.board,
      solvedBoard: state.currentActivity.solvedBoard,
      blockSizeHorizontal: state.currentActivity.blockSizeHorizontal,
      blockSizeVertical: state.currentActivity.blockSizeVertical,
      boardSize: state.currentActivity.boardSize,
      // Game data
      shuffledCellValues: state.currentActivity.shuffledCellValues,
      boardConflicts: state.currentActivity.boardConflicts,
      selectedCell: state.currentActivity.selectedCell,
      showConflicts: state.currentActivity.showConflicts,
      givenTipsCount: state.currentActivity.givenTipsCount,
      buttonTipsCountdown: state.currentActivity.buttonTipsCountdown,
    );
    // game.dump();

    updateState(activity);
  }

  void startNewActivity(BuildContext context) {
    final ActivitySettingsCubit activitySettingsCubit =
        BlocProvider.of<ActivitySettingsCubit>(context);

    final Activity newActivity = Activity.createNew(
      // Settings
      activitySettings: activitySettingsCubit.state.settings,
    );

    newActivity.dump();

    updateState(newActivity);
    refresh();

    BoardAnimate.startAnimation(this, 'start');
  }

  void selectCell(CellLocation location) {
    state.currentActivity.selectedCell = state.currentActivity.board.get(location);
    refresh();
  }

  void unselectCell() {
    state.currentActivity.selectedCell = null;
    refresh();
  }

  void updateCellValue(CellLocation location, int value) {
    if (state.currentActivity.board.get(location).isFixed == false) {
      state.currentActivity.board.set(
        location,
        Cell(
          location: location,
          value: value,
          isFixed: false,
        ),
      );
      state.currentActivity.isStarted = true;
      refresh();
    }

    if (state.currentActivity.checkBoardIsSolved()) {
      BoardAnimate.startAnimation(this, 'win');
      state.currentActivity.isFinished = true;
      refresh();
    }
  }

  void toggleShowConflicts() {
    state.currentActivity.showConflicts = !state.currentActivity.showConflicts;
    refresh();
  }

  void increaseGivenTipsCount() {
    state.currentActivity.givenTipsCount++;
    state.currentActivity.buttonTipsCountdown =
        ApplicationConfig.defaultTipCountDownValueInSeconds;
    refresh();

    const Duration interval = Duration(milliseconds: 500);
    Timer.periodic(
      interval,
      (Timer timer) {
        if (state.currentActivity.buttonTipsCountdown == 0) {
          timer.cancel();
        } else {
          state.currentActivity.buttonTipsCountdown =
              max(state.currentActivity.buttonTipsCountdown - 1, 0);
        }
        refresh();
      },
    );
  }

  bool canBeResumed() {
    return state.currentActivity.canBeResumed;
  }

  void quitActivity() {
    state.currentActivity.isRunning = false;
    refresh();
  }

  void resumeSavedActivity() {
    state.currentActivity.isRunning = true;
    refresh();
  }

  void deleteSavedActivity() {
    state.currentActivity.isRunning = false;
    state.currentActivity.isFinished = true;
    refresh();
  }

  void updateAnimationInProgress(bool animationInProgress) {
    state.currentActivity.animationInProgress = animationInProgress;
    refresh();
  }

  void setAnimatedBackground(List animatedCellsPattern) {
    for (int row = 0; row < state.currentActivity.boardSize; row++) {
      for (int col = 0; col < state.currentActivity.boardSize; col++) {
        state.currentActivity.boardAnimated[row][col] = animatedCellsPattern[row][col];
      }
    }
    refresh();
  }

  void resetAnimatedBackground() {
    for (int row = 0; row < state.currentActivity.boardSize; row++) {
      for (int col = 0; col < state.currentActivity.boardSize; col++) {
        state.currentActivity.boardAnimated[row][col] = false;
      }
    }
    refresh();
  }

  @override
  ActivityState? fromJson(Map<String, dynamic> json) {
    final Activity currentActivity = json['currentActivity'] as Activity;

    return ActivityState(
      currentActivity: currentActivity,
    );
  }

  @override
  Map<String, dynamic>? toJson(ActivityState state) {
    return <String, dynamic>{
      'currentActivity': state.currentActivity.toJson(),
    };
  }
}

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/config/application_config.dart';
import 'package:sudoku/cubit/activity/activity_cubit.dart';
import 'package:sudoku/ui/skeleton.dart';

void main() async {
  // Initialize packages
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  final Directory tmpDir = await getTemporaryDirectory();
  Hive.init(tmpDir.toString());
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: tmpDir,
  );

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((value) => runApp(EasyLocalization(
            path: 'assets/translations',
            supportedLocales: const <Locale>[
              Locale('en'),
              Locale('fr'),
            ],
            fallbackLocale: const Locale('en'),
            useFallbackTranslations: true,
            child: const MyApp(),
          )));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final List<String> assets = getImagesAssets();
    for (String asset in assets) {
      precacheImage(AssetImage(asset), context);
    }

    return MultiBlocProvider(
      providers: [
        // default providers
        BlocProvider<NavCubitPage>(
          create: (context) => NavCubitPage(appConfig: ApplicationConfig.config),
        ),
        BlocProvider<NavCubitScreen>(
          create: (context) => NavCubitScreen(),
        ),
        BlocProvider<ApplicationThemeModeCubit>(
          create: (context) => ApplicationThemeModeCubit(),
        ),
        BlocProvider<ActivityCubit>(
          create: (context) => ActivityCubit(),
        ),
        BlocProvider<ActivitySettingsCubit>(
          create: (context) => ActivitySettingsCubit(appConfig: ApplicationConfig.config),
        ),
      ],
      child: BlocBuilder<ApplicationThemeModeCubit, ApplicationThemeModeState>(
        builder: (BuildContext context, ApplicationThemeModeState state) {
          return MaterialApp(
            title: ApplicationConfig.config.appTitle,
            home: const SkeletonScreen(),

            // Theme stuff
            theme: lightTheme,
            darkTheme: darkTheme,
            themeMode: state.themeMode,

            // Localization stuff
            localizationsDelegates: context.localizationDelegates,
            supportedLocales: context.supportedLocales,
            locale: context.locale,
            debugShowCheckedModeBanner: false,
          );
        },
      ),
    );
  }

  List<String> getImagesAssets() {
    final List<String> assets = [];

    const List<String> gameImages = [
      'button_help',
      'button_show_conflicts',
      'cell_empty',
      'game_win',
      'placeholder',
    ];

    for (String image in gameImages) {
      assets.add('assets/ui/$image.png');
    }

    List<String> skinImages = [];
    for (int value = 1; value <= 16; value++) {
      skinImages.add(value.toString());
    }

    for (String skin in ApplicationConfig.config
        .getFromCode(ApplicationConfig.parameterCodeSkin)
        .allowedValues) {
      assets.add('assets/ui/skin_$skin.png');
      for (String image in skinImages) {
        assets.add('assets/skins/${skin}_$image.png');
      }
    }

    return assets;
  }
}

import 'dart:async';

import 'package:sudoku/cubit/activity/activity_cubit.dart';
import 'package:sudoku/models/activity/activity.dart';
import 'package:sudoku/models/activity/types.dart';

class BoardAnimate {
  // Start game animation: blinking tiles
  static AnimatedBoardSequence createStartGameAnimationPatterns(Activity activity) {
    AnimatedBoardSequence patterns = [];

    int patternsCount = 3;

    for (int patternIndex = 0; patternIndex < patternsCount; patternIndex++) {
      AnimatedBoard pattern = [];
      for (int row = 0; row < activity.boardSize; row++) {
        List<bool> patternRow = [];
        for (int col = 0; col < activity.boardSize; col++) {
          patternRow.add(((patternIndex + row + col) % 2 == 0));
        }
        pattern.add(patternRow);
      }
      patterns.add(pattern);
    }

    return patterns;
  }

  // Win game animation: fill board with colored rows, from bottom to top
  static AnimatedBoardSequence createWinGameAnimationPatterns(Activity activity) {
    AnimatedBoardSequence patterns = [];

    int patternsCount = activity.boardSize + 6;

    for (int patternIndex = 0; patternIndex < patternsCount; patternIndex++) {
      AnimatedBoard pattern = [];
      for (int row = 0; row < activity.boardSize; row++) {
        List<bool> patternRow = [];
        for (int col = 0; col < activity.boardSize; col++) {
          patternRow.add(row > (patternIndex - 4));
        }
        pattern.add(patternRow);
      }
      patterns.add(pattern);
    }

    return patterns;
  }

  // Default multi-purpose animation: sliding stripes, from top left to right bottom
  static AnimatedBoardSequence createDefaultAnimationPatterns(Activity activity) {
    AnimatedBoardSequence patterns = [];

    int boardSideLength = activity.boardSize;
    int patternsCount = boardSideLength;

    for (int patternIndex = 0; patternIndex < patternsCount; patternIndex++) {
      AnimatedBoard pattern = [];
      for (int row = 0; row < activity.boardSize; row++) {
        List<bool> patternRow = [];
        for (int col = 0; col < activity.boardSize; col++) {
          patternRow.add(((patternIndex + row + col) % 4 == 0));
        }
        pattern.add(patternRow);
      }
      patterns.add(pattern);
    }

    return patterns;
  }

  static void startAnimation(ActivityCubit activityCubit, String animationType) {
    final Activity activity = activityCubit.state.currentActivity;

    AnimatedBoardSequence patterns = [];

    switch (animationType) {
      case 'start':
        patterns = createStartGameAnimationPatterns(activity);
        break;
      case 'win':
        patterns = createWinGameAnimationPatterns(activity);
        break;
      default:
        patterns = createDefaultAnimationPatterns(activity);
    }

    int patternIndex = patterns.length;

    activityCubit.updateAnimationInProgress(true);

    const interval = Duration(milliseconds: 200);
    Timer.periodic(
      interval,
      (Timer timer) {
        if (patternIndex == 0) {
          timer.cancel();
          activityCubit.resetAnimatedBackground();
          activityCubit.updateAnimationInProgress(false);
        } else {
          patternIndex--;
          activityCubit.setAnimatedBackground(patterns[patternIndex]);
        }
      },
    );
  }
}

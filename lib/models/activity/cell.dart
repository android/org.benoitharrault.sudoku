import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/models/activity/cell_location.dart';

class Cell {
  const Cell({
    required this.location,
    required this.value,
    required this.isFixed,
  });

  final CellLocation location;
  final int value;
  final bool isFixed;

  static Cell none = Cell(
    location: CellLocation.go(0, 0),
    value: 0,
    isFixed: true,
  );

  void dump() {
    printlog('$Cell:');
    printlog('  location: $location');
    printlog('  value: $value');
    printlog('  isFixed: $isFixed');
    printlog('');
  }

  @override
  String toString() {
    return '$Cell(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'location': location.toJson(),
      'value': value,
      'isFixed': isFixed,
    };
  }
}

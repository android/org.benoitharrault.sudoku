import 'dart:math';

import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:sudoku/config/application_config.dart';
import 'package:sudoku/cubit/activity/activity_cubit.dart';
import 'package:sudoku/data/game_data.dart';
import 'package:sudoku/models/activity/board.dart';
import 'package:sudoku/models/activity/cell.dart';
import 'package:sudoku/models/activity/cell_location.dart';
import 'package:sudoku/models/activity/types.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.isFinished = false,
    this.animationInProgress = false,
    this.boardAnimated = const [],

    // Base data
    required this.board,
    required this.solvedBoard,
    required this.blockSizeHorizontal,
    required this.blockSizeVertical,
    required this.boardSize,

    // Game data
    this.shuffledCellValues = const [],
    this.boardConflicts = const [],
    this.selectedCell,
    this.showConflicts = false,
    this.givenTipsCount = 0,
    this.buttonTipsCountdown = 0,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool isFinished;
  bool animationInProgress;
  AnimatedBoard boardAnimated;

  // Base data
  final Board board;
  final Board solvedBoard;
  final int blockSizeHorizontal;
  final int blockSizeVertical;
  final int boardSize;

  // Game data
  List<int> shuffledCellValues;
  ConflictsCount boardConflicts;
  Cell? selectedCell;
  bool showConflicts;
  int givenTipsCount;
  int buttonTipsCountdown;

  factory Activity.createEmpty() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      board: Board.createEmpty(),
      solvedBoard: Board.createEmpty(),
      blockSizeHorizontal: 0,
      blockSizeVertical: 0,
      boardSize: 0,
      // Game data
      shuffledCellValues: [],
      givenTipsCount: 0,
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    final int blockSizeHorizontal = int.parse(
        newActivitySettings.get(ApplicationConfig.parameterCodeBoardSize).split('x')[0]);
    final int blockSizeVertical = int.parse(
        newActivitySettings.get(ApplicationConfig.parameterCodeBoardSize).split('x')[1]);
    final int boardSize = blockSizeHorizontal * blockSizeVertical;

    const int maxCellValue = 16;
    final List<int> shuffledCellValues = List<int>.generate(maxCellValue, (i) => i + 1);

    if (ApplicationConfig.shufflableSkins
        .contains(newActivitySettings.get(ApplicationConfig.parameterCodeSkin))) {
      shuffledCellValues.shuffle();
      printlog('Shuffled tiles values: $shuffledCellValues');
    }

    ConflictsCount nonConflictedBoard = [];

    for (int row = 0; row < boardSize; row++) {
      List<int> line = [];
      for (int col = 0; col < boardSize; col++) {
        line.add(0);
      }
      nonConflictedBoard.add(line);
    }

    final List<String> templates =
        GameData.templates[newActivitySettings.get(ApplicationConfig.parameterCodeBoardSize)]
                ?[newActivitySettings.get(ApplicationConfig.parameterCodeDifficultyLevel)] ??
            [];
    final String template = templates.elementAt(Random().nextInt(templates.length)).toString();

    if (template.length != pow(blockSizeHorizontal * blockSizeVertical, 2)) {
      printlog('Failed to get grid template...');
      return Activity.createEmpty();
    }

    final Board board = Board.createFromTemplate(
      template: template,
      isSymetric: (blockSizeHorizontal == blockSizeVertical),
    );
    final Board solvedBoard = Board.createNew(cells: board.getSolvedGrid());

    // Animated background
    AnimatedBoard notAnimatedBoard = [];
    for (int row = 0; row < boardSize; row++) {
      List<bool> line = [];
      for (int col = 0; col < boardSize; col++) {
        line.add(false);
      }
      notAnimatedBoard.add(line);
    }

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      boardAnimated: notAnimatedBoard,
      // Base data
      board: board,
      solvedBoard: solvedBoard,
      blockSizeHorizontal: blockSizeHorizontal,
      blockSizeVertical: blockSizeVertical,
      boardSize: boardSize,
      // Game data
      shuffledCellValues: shuffledCellValues,
      boardConflicts: nonConflictedBoard,
      selectedCell: null,
    );
  }

  bool get canBeResumed => isStarted && !isFinished;

  bool get gameWon => isRunning && isStarted && isFinished;

  bool get canGiveTip => (buttonTipsCountdown == 0);

  int getTranslatedValueForDisplay(int originalValue) {
    return shuffledCellValues[originalValue - 1];
  }

  bool checkBoardIsSolved() {
    // (re)compute conflicts
    boardConflicts = computeConflictsInBoard();

    // check grid is fully completed and does not contain conflict
    for (int row = 0; row < boardSize; row++) {
      for (int col = 0; col < boardSize; col++) {
        if (board.cells[row][col].value == 0 || boardConflicts[row][col] != 0) {
          return false;
        }
      }
    }

    printlog('-> ok sudoku solved!');

    return true;
  }

  ConflictsCount computeConflictsInBoard() {
    final BoardCells cells = board.cells;
    final ConflictsCount conflicts = boardConflicts;

    // reset conflict states
    for (int row = 0; row < boardSize; row++) {
      for (int col = 0; col < boardSize; col++) {
        conflicts[row][col] = 0;
      }
    }

    // check lines does not contains a value twice
    for (int row = 0; row < boardSize; row++) {
      final List<int> values = [];
      for (int col = 0; col < boardSize; col++) {
        int value = cells[row][col].value;
        if (value != 0) {
          values.add(value);
        }
      }
      final List<int> distinctValues = values.toSet().toList();
      if (values.length != distinctValues.length) {
        printlog('line $row contains duplicates');
        // Add line to cells in conflict
        for (int col = 0; col < boardSize; col++) {
          conflicts[row][col]++;
        }
      }
    }

    // check columns does not contains a value twice
    for (int col = 0; col < boardSize; col++) {
      final List<int> values = [];
      for (int row = 0; row < boardSize; row++) {
        int value = cells[row][col].value;
        if (value != 0) {
          values.add(value);
        }
      }
      final List<int> distinctValues = values.toSet().toList();
      if (values.length != distinctValues.length) {
        printlog('column $col contains duplicates');
        // Add column to cells in conflict
        for (int row = 0; row < boardSize; row++) {
          conflicts[row][col]++;
        }
      }
    }

    // check blocks does not contains a value twice
    final int horizontalBlocksCount = blockSizeVertical;
    final int verticalBlocksCount = blockSizeHorizontal;
    for (int blockRow = 0; blockRow < verticalBlocksCount; blockRow++) {
      for (int blockCol = 0; blockCol < horizontalBlocksCount; blockCol++) {
        List<int> values = [];

        for (int rowInBlock = 0; rowInBlock < blockSizeVertical; rowInBlock++) {
          for (int colInBlock = 0; colInBlock < blockSizeHorizontal; colInBlock++) {
            int row = (blockRow * blockSizeVertical) + rowInBlock;
            int col = (blockCol * blockSizeHorizontal) + colInBlock;
            int value = cells[row][col].value;
            if (value != 0) {
              values.add(value);
            }
          }
        }

        List<int> distinctValues = values.toSet().toList();
        if (values.length != distinctValues.length) {
          printlog('block [$blockCol,$blockRow] contains duplicates');
          // Add blocks to cells in conflict
          for (int rowInBlock = 0; rowInBlock < blockSizeVertical; rowInBlock++) {
            for (int colInBlock = 0; colInBlock < blockSizeHorizontal; colInBlock++) {
              int row = (blockRow * blockSizeVertical) + rowInBlock;
              int col = (blockCol * blockSizeHorizontal) + colInBlock;
              conflicts[row][col]++;
            }
          }
        }
      }
    }

    return conflicts;
  }

  void showTip(ActivityCubit activityCubit) {
    if (selectedCell == null) {
      // no selected cell -> pick one
      helpSelectCell(activityCubit);
    } else {
      // currently selected cell -> set value
      helpFillCell(activityCubit);
    }
    activityCubit.increaseGivenTipsCount();
  }

  void helpSelectCell(ActivityCubit activityCubit) {
    // pick one of wrong value cells, if found
    final List<List<int>> wrongValueCells = getCellsWithWrongValue();
    if (wrongValueCells.isNotEmpty) {
      pickRandomFromList(activityCubit, wrongValueCells);
      return;
    }

    // pick one of conflicting cells, if found
    final List<List<int>> conflictingCells = getCellsWithConflicts();
    if (conflictingCells.isNotEmpty) {
      pickRandomFromList(activityCubit, conflictingCells);
      return;
    }

    //  pick one form cells with unique non-conflicting candidate value
    final List<List<int>> candidateCells = board.getEmptyCellsWithUniqueAvailableValue();
    if (candidateCells.isNotEmpty) {
      pickRandomFromList(activityCubit, candidateCells);
      return;
    }
  }

  List<List<int>> getCellsWithWrongValue() {
    final BoardCells cells = board.cells;
    final BoardCells cellsSolved = solvedBoard.cells;

    List<List<int>> cellsWithWrongValue = [];

    for (int row = 0; row < boardSize; row++) {
      for (int col = 0; col < boardSize; col++) {
        if (cells[row][col].value != 0 &&
            cells[row][col].value != cellsSolved[row][col].value) {
          cellsWithWrongValue.add([row, col]);
        }
      }
    }

    return cellsWithWrongValue;
  }

  List<List<int>> getCellsWithConflicts() {
    List<List<int>> cellsWithConflict = [];

    for (int row = 0; row < boardSize; row++) {
      for (int col = 0; col < boardSize; col++) {
        if (boardConflicts[row][col] != 0) {
          cellsWithConflict.add([row, col]);
        }
      }
    }

    return cellsWithConflict;
  }

  void pickRandomFromList(ActivityCubit activityCubit, List<List<int>> cellsCoordinates) {
    if (cellsCoordinates.isNotEmpty) {
      cellsCoordinates.shuffle();
      final List<int> cell = cellsCoordinates[0];
      activityCubit.selectCell(CellLocation.go(cell[0], cell[1]));
    }
  }

  void helpFillCell(ActivityCubit activityCubit) {
    // Will clean cell if no eligible value found
    int eligibleValue = 0;

    // Ensure there is only one eligible value for this cell
    int allowedValuesCount = 0;
    for (int value = 1; value <= boardSize; value++) {
      if (board.isValueAllowed(selectedCell?.location, value)) {
        allowedValuesCount++;
        eligibleValue = value;
      }
    }

    activityCubit.updateCellValue(
        selectedCell!.location, allowedValuesCount == 1 ? eligibleValue : 0);
    activityCubit.unselectCell();
  }

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    printlog('  Base data');
    printlog('    blockSizeHorizontal: $blockSizeHorizontal');
    printlog('    blockSizeVertical: $blockSizeVertical');
    printlog('    boardSize: $boardSize');
    printlog('  Game data');
    printlog('    shuffledCellValues: $shuffledCellValues');
    printlog('    selectedCell: ${selectedCell?.toString() ?? ''}');
    printlog('    showConflicts: $showConflicts');
    printlog('    givenTipsCount: $givenTipsCount');
    printlog('    buttonTipsCountdown: $buttonTipsCountdown');
    printlog('');

    printGrid();
    printlog('');
  }

  printGrid() {
    final BoardCells cells = board.cells;
    final BoardCells solvedCells = solvedBoard.cells;

    const String stringValues = '0123456789ABCDEFG';
    printlog('');
    printlog('-------');
    for (int rowIndex = 0; rowIndex < cells.length; rowIndex++) {
      String row = '';
      String rowSolved = '';
      for (int colIndex = 0; colIndex < cells[rowIndex].length; colIndex++) {
        row += stringValues[cells[rowIndex][colIndex].value];
        rowSolved += stringValues[solvedCells[rowIndex][colIndex].value];
      }
      printlog('$row | $rowSolved');
    }
    printlog('-------');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      'boardAnimated': boardAnimated,
      // Base data
      'board': board.toJson(),
      'solvedBoard': solvedBoard.toJson(),
      'blockSizeHorizontal': blockSizeHorizontal,
      'blockSizeVertical': blockSizeVertical,
      'boardSize': boardSize,
      // Game data
      'shuffledCellValues': shuffledCellValues,
      'boardConflicts': boardConflicts,
      'selectedCell': selectedCell?.toJson(),
      'showConflicts': showConflicts,
      'givenTipsCount': givenTipsCount,
      'buttonTipsCountdown': buttonTipsCountdown,
    };
  }
}
